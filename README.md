# README #

NAMED PIPE example on Python 2.7 in Linux variants (not windows).

### What is this repository for? ###

* Tutorial how to implement simple named pipe between two different process using Named Pipe.
* Please take note, the female.py do read the name pipe as NON BLOCKING. to reserve your cpu process, do play with the sleep function.

### How do I get set up? ###

* Install python inside Ubuntu or unix/linux variant
* Run female.py on seperate terminal, and male.py on another terminal. Make sure female must act first!
* No dependencies. only user permission on /tmp

### Contribution guidelines ###

* Make sure ur coding is understandable by me.

### Who do I talk to? ###

* Eddy Irwan
* eddyirwan@gmail.com

[Buy me a coffee please..](https://ko-fi.com/A8504525)